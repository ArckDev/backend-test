/*
 * Property of arck23@gmail.com
 * Class AbstractIntegrationTests.java created on 24 ene. 2020 18:23:07
 */
package com.altran.backend.integration.orm;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import com.altran.backend.AbstractTests;
import com.altran.backend.integration.orm.dao.PackageRepository;
import com.altran.backend.integration.orm.entity.PackageEntity;

/**
 * Abstract class to provide the configuration for the orm layer tests and inherited from the main
 * configuration of {@link AbstractTests}
 * 
 * @author Jose
 * @since 1.0.0
 */
public abstract class AbstractOrmTests extends AbstractTests {

	protected static final int ENTRIES_NUMBER = 10;

	@Autowired
	PackageRepository packageRepository;


	@Before
	public void createEntriesToTest() {

		// Clean the repository
		this.packageRepository.deleteAll();

		final List<PackageEntity> packageEntityList = new ArrayList<>();

		for (int i = 0; i < ENTRIES_NUMBER; i++) {
			final PackageEntity packageEntity = new PackageEntity();
			packageEntity.setCode("code" + i);
			packageEntity.setOrganizationDescription("description" + i);
			packageEntity.setUrl("www.test" + i + ".com");

			packageEntityList.add(packageEntity);
		}

		this.packageRepository.saveAll(packageEntityList);
	}

}

