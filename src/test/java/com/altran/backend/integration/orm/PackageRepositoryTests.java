/*
 * Property of arck23@gmail.com
 * Class PackageRepositoryTests.java created on 24 ene. 2020 0:09:16
 */
package com.altran.backend.integration.orm;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import com.altran.backend.integration.orm.dao.PackageRepository;
import com.altran.backend.integration.orm.entity.PackageEntity;

/**
 * Class to perform the tests for the {@link PackageRepository} class and configured by
 * {@link AbstractOrmTests}
 * 
 * @author Jose
 * @since 1.0.0
 */
public class PackageRepositoryTests extends AbstractOrmTests {


	@Test
	public void getAllEntriesShouldReturnEntriesNumber() {

		final List<PackageEntity> allEntries = this.packageRepository.findAll();

		assertEquals(ENTRIES_NUMBER, allEntries.size());
	}


	@Test
	public void saveEntryShouldWork() {

		final PackageEntity packageEntity = new PackageEntity();
		packageEntity.setCode("code");
		packageEntity.setOrganizationDescription("description");
		packageEntity.setUrl("www.test.com");

		final PackageEntity savedEntity = this.packageRepository.save(packageEntity);

		assertEquals(packageEntity.getCode(), savedEntity.getCode());
		assertEquals(packageEntity.getOrganizationDescription(), savedEntity.getOrganizationDescription());
		assertEquals(packageEntity.getUrl(), savedEntity.getUrl());
	}


	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void saveNullEntryShouldTrowException() {

		this.packageRepository.save(null);
	}

}

