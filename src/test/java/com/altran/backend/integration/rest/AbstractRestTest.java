/*
 * Property of arck23@gmail.com
 * Class AbstractRestTest.java created on 24 ene. 2020 19:04:48
 */
package com.altran.backend.integration.rest;

import com.altran.backend.AbstractTests;

/**
 * Abstract class to provide the configuration for the rest layer tests and inherited from the main
 * configuration of {@link AbstractTests}
 * 
 * @author Jose
 * @since 1.0.0
 */
public abstract class AbstractRestTest extends AbstractTests {

}

