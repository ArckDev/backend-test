/*
 * Property of arck23@gmail.com
 * Class RestServiceIntegrationTests.java created on 24 ene. 2020 19:05:46
 */
package com.altran.backend.integration.rest;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.altran.backend.integration.rest.dto.PackageListRestDto;
import com.altran.backend.integration.rest.service.RestServiceIntegration;

/**
 * Class to perform the tests for the {@link RestServiceIntegration} class and configured by
 * {@link AbstractRestTest}
 * 
 * @author Jose
 * @since 1.0.0
 */
public class RestServiceIntegrationTests extends AbstractRestTest {

	@Autowired
	RestServiceIntegration restServiceIntegration;


	/**
	 * This is not a unit test, it's an integration test. Due the simplicity of the method we can't
	 * perform a unit test here because the only call must be mocked
	 */
	@Test
	public void getPackagesShouldWork() {

		final PackageListRestDto packages = this.restServiceIntegration.getPackages();

		assertNotNull(packages);
		assertTrue(packages.getResult().getResults().size() > 0);
	}

}

