/*
 * Property of arck23@gmail.com
 * Class BaseTest.java created on 24 ene. 2020 18:17:31
 */
package com.altran.backend;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Abstract class to provide the configuration for all the tests
 * 
 * @author Jose
 * @since 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class AbstractTests {

}

