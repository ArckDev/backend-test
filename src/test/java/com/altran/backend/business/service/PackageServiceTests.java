/*
 * Property of arck23@gmail.com
 * Class PackageServiceTests.java created on 24 ene. 2020 18:16:53
 */
package com.altran.backend.business.service;

import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.altran.backend.business.mapper.PackageListRestDtoToPackageDtoMapper;
import com.altran.backend.business.service.impl.PackageServiceImpl;
import com.altran.backend.integration.rest.dto.PackageListRestDto;
import com.altran.backend.integration.rest.dto.PackageListRestDto.Organization;
import com.altran.backend.integration.rest.dto.PackageListRestDto.PackageElement;
import com.altran.backend.integration.rest.dto.PackageListRestDto.ResultList;
import com.altran.backend.integration.rest.dto.PackageListRestDto.Url;
import com.altran.backend.integration.rest.service.RestServiceIntegration;

/**
 * Class to perform the tests for the {@link PackageService} class and configured by
 * {@link AbstractBusinessTests}
 * 
 * @author Jose
 * @since 1.0.0
 */
public class PackageServiceTests extends AbstractBusinessTests {

	@Mock
	PackageListRestDtoToPackageDtoMapper mapperRest;

	@InjectMocks
	PackageServiceImpl packageService;

	@Mock
	RestServiceIntegration restServiceIntegration;


	/**
	 * This test is not useful due the simplicity of the method to test. All the calls must be
	 * mocked because there are no logic inside the method. It's created only as example of using
	 * Mocks
	 */
	@Test
	public void getPackagesFromIntegrationServiceAndStoreLocallyShouldWork() {

		when(this.restServiceIntegration.getPackages()).thenReturn(this.initPackageListRestDto());

		// this.packageService.getPackagesFromIntegrationServiceAndStoreLocally();
	}


	private PackageListRestDto initPackageListRestDto() {

		final Organization organization = new Organization();
		organization.setDescription("Description");

		final Url url = new Url();
		url.setUrlCatala("www.test.com");

		final PackageElement packageElement = new PackageElement();
		packageElement.setCode("code");
		packageElement.setUrl(url);
		packageElement.setOrganization(organization);

		final ResultList resultList = new ResultList();
		resultList.setResults(Arrays.asList(packageElement));

		final PackageListRestDto packageListRestDto = new PackageListRestDto();
		packageListRestDto.setResult(resultList);

		return packageListRestDto;
	}


}

