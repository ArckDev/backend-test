/*
 * Property of arck23@gmail.com
 * Class AbstractBusinessTest.java created on 24 ene. 2020 18:20:44
 */
package com.altran.backend.business.service;

import com.altran.backend.AbstractTests;

/**
 * Abstract class to provide the configuration for the business layer tests and inherited from the
 * main configuration of {@link AbstractTests}
 * 
 * @author Jose
 * @since 1.0.0
 */
public abstract class AbstractBusinessTests extends AbstractTests {

}

