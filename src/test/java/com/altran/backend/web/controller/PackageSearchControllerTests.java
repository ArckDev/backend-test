/*
 * Property of arck23@gmail.com
 * Class PackageSearchControllerTests.java created on 25 ene. 2020 18:27:02
 */
package com.altran.backend.web.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.altran.backend.web.restcontroller.PackageSearchController;

/**
 * Class to perform the tests for the {@link PackageSearchController} class and configured by
 * {@link AbstractWebTests}
 * 
 * @author Jose
 * @since 1.0.0
 */
public class PackageSearchControllerTests extends AbstractWebTests {

	@Rule
	public JUnitRestDocumentation jUnitRestDocumentation = new JUnitRestDocumentation();

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;


	@Test
	public void getPackageSearchShouldReturnOk() throws Exception {
		this.mockMvc.perform(RestDocumentationRequestBuilders.get("/package-search/get-packages")) //
		        .andExpect(status().isOk()) //
		        .andExpect(content().contentType("application/json")) //
		        .andDo(document("packages/get-all"));
	}


	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
		        .apply(documentationConfiguration(this.jUnitRestDocumentation)) //
		        .build();
	}


}

