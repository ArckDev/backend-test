/*
 * Property of arck23@gmail.com
 * Class AbstractWebTests.java created on 25 ene. 2020 18:27:42
 */
package com.altran.backend.web.controller;

import com.altran.backend.AbstractTests;

/**
 * Abstract class to provide the configuration for the web layer tests and inherited from the main
 * configuration of {@link AbstractTests}
 * 
 * @author Jose
 * @since 1.0.0
 */
public abstract class AbstractWebTests extends AbstractTests {

}

