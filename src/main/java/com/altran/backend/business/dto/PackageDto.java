/*
 * Property of arck23@gmail.com
 * Class PackageDTO.java created on 23 ene. 2020 20:26:33
 */
package com.altran.backend.business.dto;

import java.io.Serializable;

public class PackageDto implements Serializable {

	public static class Builder {

		private String code;

		private String organizationDescription;

		private String url;


		public PackageDto build() {
			return new PackageDto(this);
		}


		public Builder setCode(final String code) {
			this.code = code;
			return this;
		}


		public Builder setOrganizationDescription(final String organizationDescription) {
			this.organizationDescription = organizationDescription;
			return this;
		}


		public Builder setUrl(final String url) {
			this.url = url;
			return this;
		}


	}


	private static final long serialVersionUID = 8709464948704916377L;

	private String code;

	private Long id;

	private String organizationDescription;

	private String url;


	public PackageDto() {
		super();
	}


	private PackageDto(final Builder builder) {
		super();
		this.code = builder.code;
		this.organizationDescription = builder.organizationDescription;
		this.url = builder.url;
	}


	public String getCode() {
		return this.code;
	}


	public Long getId() {
		return this.id;
	}


	public String getOrganizationDescription() {
		return this.organizationDescription;
	}


	public String getUrl() {
		return this.url;
	}


	public void setCode(final String code) {
		this.code = code;
	}


	public void setId(final Long id) {
		this.id = id;
	}


	public void setOrganizationDescription(final String organizationDescription) {
		this.organizationDescription = organizationDescription;
	}


	public void setUrl(final String url) {
		this.url = url;
	}


	@Override
	public String toString() {
		return "PackageDTO [id=" + this.id + ", code=" + this.code + ", organizationDescription="
		        + this.organizationDescription + ", uri=" + this.url + "]";
	}


}

