/*
 * Property of arck23@gmail.com
 * Class PackageService.java created on 23 ene. 2020 20:23:34
 */
package com.altran.backend.business.service;

import java.util.List;

import com.altran.backend.business.dto.PackageDto;

/**
 * 
 * Service to perform the main logic about the Packages.
 * 
 * @author Jose
 * @since TODO insert current version here
 */
public interface PackageService {

	/**
	 * 
	 * Gets the available Packages from the external API REST, stores them locally and returns them
	 * 
	 * @return {@link PackageDto} list
	 */
	List<PackageDto> getPackagesFromIntegrationServiceAndStoreLocally();


}

