/*
 * Property of arck23@gmail.com
 * Class PackageServiceImpl.java created on 23 ene. 2020 20:30:22
 */
package com.altran.backend.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altran.backend.business.dto.PackageDto;
import com.altran.backend.business.mapper.PackageDtoPackageEntityMapper;
import com.altran.backend.business.mapper.PackageListRestDtoToPackageDtoMapper;
import com.altran.backend.business.service.PackageService;
import com.altran.backend.integration.orm.dao.PackageRepository;
import com.altran.backend.integration.rest.dto.PackageListRestDto;
import com.altran.backend.integration.rest.service.RestServiceIntegration;

@Service
public class PackageServiceImpl implements PackageService {

	@Autowired
	PackageDtoPackageEntityMapper mapperBusiness;

	@Autowired
	PackageListRestDtoToPackageDtoMapper mapperRest;

	@Autowired
	PackageRepository packageRepository;

	@Autowired
	RestServiceIntegration restServiceIntegration;


	@Override
	public List<PackageDto> getPackagesFromIntegrationServiceAndStoreLocally() {

		// Get the package list from the integration service
		final PackageListRestDto packageListRestDto = this.restServiceIntegration.getPackages();

		List<PackageDto> entityToDto = null;
		if (packageListRestDto != null) {

			// Transform into layer object
			final List<PackageDto> packageDtoList = this.mapperRest.restDtoToDto(packageListRestDto);

			// Here we have a lot of possibilities:
			// 1. delete and save all the element each time,
			// 2. compare element by element of the db and update them if there are differences,
			// 3. make only an update of all the elements once a day,
			// 4. caching the results and update db if there difference with the cached elements,
			// ...
			this.packageRepository.deleteAll();

			// Persist the object in DB
			this.packageRepository.saveAll(this.mapperBusiness.dtoToEntity(packageDtoList));

			// Check the db working and return the results
			entityToDto = this.mapperBusiness.entityToDto(this.packageRepository.findAll());
		}

		return entityToDto;
	}

}
