/*
 * Property of arck23@gmail.com
 * Class PackageDTOPackage.java created on 23 ene. 2020 21:59:03
 */
package com.altran.backend.business.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.altran.backend.business.dto.PackageDto;
import com.altran.backend.integration.orm.entity.PackageEntity;

/**
 * Mapper dedicated to perform the mapping between {@link PackageDto} and {@link PackageEntity}
 * 
 * @author Jose
 * @since 1.0.0
 */
@Mapper(componentModel = "spring")
public interface PackageDtoPackageEntityMapper {

	/**
	 * Mapping from List<{@link PackageDto}> -> List<{@link PackageEntity}>
	 * 
	 * @param packageDtoList
	 * @return List<{@link PackageEntity}>
	 */
	List<PackageEntity> dtoToEntity(List<PackageDto> packageDtoList);


	/**
	 * Mapping from {@link PackageDto} -> {@link PackageEntity}
	 * 
	 * @param packageDto
	 * @return {@link PackageEntity}
	 */
	PackageEntity dtoToEntity(PackageDto packageDto);


	/**
	 * Mapping from List<{@link PackageEntity}> -> List<{@link PackageDto}>
	 * 
	 * @param packageEntityList
	 * @return List<{@link PackageDto}>
	 */
	List<PackageDto> entityToDto(List<PackageEntity> packageEntityList);


	/**
	 * Mapping from {@link PackageEntity} -> {@link PackageDto}
	 * 
	 * @param packageEntity
	 * @return {@link PackageDto}
	 */
	PackageDto entityToDto(PackageEntity packageEntity);

}

