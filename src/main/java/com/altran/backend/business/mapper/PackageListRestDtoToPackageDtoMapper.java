/*
 * Property of arck23@gmail.com
 * Class PackageListRestDtoToPackageDtoMapper.java created on 24 ene. 2020 18:09:24
 */
package com.altran.backend.business.mapper;

import java.util.List;

import com.altran.backend.business.dto.PackageDto;
import com.altran.backend.integration.rest.dto.PackageListRestDto;

/**
 * Mapper dedicated to perform the mapping between {@link PackageListRestDto} and
 * List<{@link PackageDto}>
 * 
 * @author Jose
 * @since 1.0.0
 */
public interface PackageListRestDtoToPackageDtoMapper {

	/**
	 * Mapping from {@link PackageListRestDto} -> List<{@link PackageDto}>
	 * 
	 * @param packageListRestDto
	 * @return List<{@link PackageDto}>
	 */
	List<PackageDto> restDtoToDto(final PackageListRestDto packageListRestDto);

}

