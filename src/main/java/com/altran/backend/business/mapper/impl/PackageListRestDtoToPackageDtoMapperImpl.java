/*
 * Property of arck23@gmail.com
 * Class PackageListDTOToPackageEntityMapper.java created on 23 ene. 2020 22:36:30
 */
package com.altran.backend.business.mapper.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.altran.backend.business.dto.PackageDto;
import com.altran.backend.business.mapper.PackageListRestDtoToPackageDtoMapper;
import com.altran.backend.integration.rest.dto.PackageListRestDto;


@Component
public class PackageListRestDtoToPackageDtoMapperImpl implements PackageListRestDtoToPackageDtoMapper {

	@Override
	public List<PackageDto> restDtoToDto(final PackageListRestDto packageListRestDto) {

		return packageListRestDto.getResult().getResults().stream() //
		        .map(packageElement -> new PackageDto.Builder() //
		                .setCode(packageElement.getCode()) //
		                .setOrganizationDescription(packageElement.getOrganization().getDescription())
		                .setUrl(packageElement.getUrl().getUrlCatala()) //
		                .build())
		        .collect(Collectors.toList());
	}


}

