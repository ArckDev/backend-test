/*
 * Property of arck23@gmail.com
 * Class PackageDTOPackage.java created on 23 ene. 2020 21:59:03
 */
package com.altran.backend.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.altran.backend.business.dto.PackageDto;
import com.altran.backend.web.dto.PackageRestDto;

/**
 * Mapper dedicated to perform the mapping between {@link PackageRestDto} and {@link PackageDto}
 * 
 * @author Jose
 * @since 1.0.0
 */
@Mapper(componentModel = "spring")
public interface PackageDtoPackageRestDtoMapper {

	/**
	 * Mapping from a list of {@link PackageDto} -> list of {@link PackageRestDto}
	 * 
	 * @param packageDtoList
	 * @return packageRestDtoList
	 */
	List<PackageRestDto> dtoToRestDto(List<PackageDto> packageDtoList);


	/**
	 * Mapping from a {@link PackageDto} -> {@link PackageRestDto}
	 * 
	 * @param packageDto
	 * @return packageRestDto
	 */
	PackageRestDto dtoToRestDto(PackageDto packageDto);

}

