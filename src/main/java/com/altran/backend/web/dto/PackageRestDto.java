/*
 * Property of arck23@gmail.com
 * Class PackageDTO.java created on 23 ene. 2020 20:26:33
 */
package com.altran.backend.web.dto;

/**
 * Data Transfer Object dedicated to transport the information about the Package to the response
 * 
 * @author Jose
 * @since 1.0.0
 */
public class PackageRestDto {

	private String code;

	private String organizationDescription;

	private String url;


	public PackageRestDto() {
		super();
	}


	public String getCode() {
		return this.code;
	}


	public String getOrganizationDescription() {
		return this.organizationDescription;
	}


	public String getUrl() {
		return this.url;
	}


	public void setCode(final String code) {
		this.code = code;
	}


	public void setOrganizationDescription(final String organizationDescription) {
		this.organizationDescription = organizationDescription;
	}


	public void setUrl(final String url) {
		this.url = url;
	}


	@Override
	public String toString() {
		return "PackageRestDto [code=" + this.code + ", organizationDescription=" + this.organizationDescription
		        + ", url=" + this.url + "]";
	}


}

