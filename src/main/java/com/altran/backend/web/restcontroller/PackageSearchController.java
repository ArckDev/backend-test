package com.altran.backend.web.restcontroller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.altran.backend.business.dto.PackageDto;
import com.altran.backend.business.service.PackageService;
import com.altran.backend.integration.orm.entity.PackageEntity;
import com.altran.backend.web.mapper.PackageDtoPackageRestDtoMapper;

/**
 * This is the controller dedicated to handle the requests about the package search which
 * information is stored in the {@link PackageEntity} objects
 * 
 * @author Jose
 * @since 1.0.0
 */
@RestController
@RequestMapping("/package-search/")
public class PackageSearchController {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PackageDtoPackageRestDtoMapper mapper;

	@Autowired
	PackageService packageService;

	@Autowired
	RestTemplate restTemplate;


	/**
	 * Gets all the available packages and return a list of them
	 * 
	 * @param request
	 * @return responseEntity
	 */
	@GetMapping("/get-packages")
	public ResponseEntity<?> getPackageSearch(final HttpServletRequest request) {

		this.logger.info("Info requested from {}", request.getRemoteAddr());

		final ResponseEntity<?> responseEntity;

		final List<PackageDto> packageDtoList = this.packageService.getPackagesFromIntegrationServiceAndStoreLocally();

		if (packageDtoList != null) {
			responseEntity = new ResponseEntity<>(this.mapper.dtoToRestDto(packageDtoList), HttpStatus.OK);
		} else {
			responseEntity =
			        new ResponseEntity<>("The server can't process your request 🙄", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}
}
