/*
 * Property of arck23@gmail.com
 * Class RestServiceIntegration.java created on 23 ene. 2020 20:13:57
 */
package com.altran.backend.integration.rest.service;

import com.altran.backend.integration.rest.dto.PackageListRestDto;

/**
 * Service dedicated to perform the integration between the system and the external API REST
 * 
 * @author Jose
 * @since 1.0.0
 */
public interface RestServiceIntegration {

	/**
	 * Retrieves all the available packages from the external API
	 * 
	 * 
	 * @return {@link PackageListRestDto}
	 */
	PackageListRestDto getPackages();
}

