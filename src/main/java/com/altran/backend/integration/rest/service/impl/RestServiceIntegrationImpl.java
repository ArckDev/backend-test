/*
 * Property of arck23@gmail.com
 * Class RestServiceIntegrationImpl.java created on 23 ene. 2020 20:20:11
 */
package com.altran.backend.integration.rest.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.altran.backend.integration.rest.dto.PackageListRestDto;
import com.altran.backend.integration.rest.service.RestServiceIntegration;


@Service
public class RestServiceIntegrationImpl implements RestServiceIntegration {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RestTemplate restTemplate;

	@Value("${package.search.url}")
	private String url;


	@Override
	public PackageListRestDto getPackages() {

		PackageListRestDto packageListRestDto = null;

		try {
			packageListRestDto = this.restTemplate.getForObject(this.url, PackageListRestDto.class);
			this.logger.info("Information obtained from {} at {}", this.url, new Date());
		} catch (final ResourceAccessException e) {
			this.logger.error("Error retrieving info from", e);
		}

		return packageListRestDto;
	}

}

