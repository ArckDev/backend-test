/*
 * Property of arck23@gmail.com
 * Class PackageDTO.java created on 22 ene. 2020 21:07:27
 */
package com.altran.backend.integration.rest.dto;

import java.util.ArrayList;
import java.util.List;

import com.altran.backend.integration.rest.service.RestServiceIntegration;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object responsible of transporting the information about the Packages from the external API REST
 * performed in {@link RestServiceIntegration}
 * 
 * @author Jose
 * @since 1.0.0
 */
public class PackageListRestDto {

	/**
	 * Organization element linked to the JSON obtained from the API
	 */
	public static class Organization {

		@JsonProperty("description")
		private String description;


		public String getDescription() {
			return this.description;
		}


		public void setDescription(final String description) {
			this.description = description;
		}


		@Override
		public String toString() {
			return "Organization [description=" + this.description + "]";
		}
	}

	/**
	 * Package element linked to the JSON obtained from the API
	 */
	public static class PackageElement {

		@JsonProperty("code")
		private String code;

		@JsonProperty("organization")
		private Organization organization;

		@JsonProperty("url_tornada")
		private Url url;


		public String getCode() {
			return this.code;
		}


		public Organization getOrganization() {
			return this.organization;
		}


		public Url getUrl() {
			return this.url;
		}


		public void setCode(final String code) {
			this.code = code;
		}


		public void setOrganization(final Organization organization) {
			this.organization = organization;
		}


		public void setUrl(final Url url) {
			this.url = url;
		}
	}

	/**
	 * List of results linked to the JSON obtained from the API
	 */
	public static class ResultList {

		@JsonProperty("results")
		private List<PackageElement> resultList = new ArrayList<>();


		public List<PackageElement> getResults() {
			return this.resultList;
		}


		public void setResults(final List<PackageElement> resultList) {
			this.resultList = resultList;
		}
	}

	/**
	 * URL element linked to the JSON obtained from the API
	 */
	public static class Url {

		@JsonProperty("ca")
		private String urlCatala;


		public String getUrlCatala() {
			return this.urlCatala;
		}


		public void setUrlCatala(final String urlCatala) {
			this.urlCatala = urlCatala;
		}


		@Override
		public String toString() {
			return "Url [urlCatala=" + this.urlCatala + "]";
		}
	}


	@JsonProperty("result")
	private ResultList result;


	public ResultList getResult() {
		return this.result;
	}


	public void setResult(final ResultList result) {
		this.result = result;
	}


}

