/*
 * Property of arck23@gmail.com
 * Class Package.java created on 23 ene. 2020 21:52:09
 */
package com.altran.backend.integration.orm.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

/**
 * Entity responsible of handling the persistence of the Package
 * 
 * @author Jose
 * @since 1.0.0
 */
@Entity
public class PackageEntity {

	private String code;

	@CreationTimestamp
	private LocalDateTime createDate;

	@Id
	@GeneratedValue
	private Long id;

	private String organizationDescription;

	private String url;


	public String getCode() {
		return this.code;
	}


	public LocalDateTime getCreateDate() {
		return this.createDate;
	}


	public Long getId() {
		return this.id;
	}


	public String getOrganizationDescription() {
		return this.organizationDescription;
	}


	public String getUrl() {
		return this.url;
	}


	public void setCode(final String code) {
		this.code = code;
	}


	public void setCreateDate(final LocalDateTime createDate) {
		this.createDate = createDate;
	}


	public void setId(final Long id) {
		this.id = id;
	}


	public void setOrganizationDescription(final String organizationDescription) {
		this.organizationDescription = organizationDescription;
	}


	public void setUrl(final String url) {
		this.url = url;
	}


	@Override
	public String toString() {
		return "PackageEntity [code=" + this.code + ", createDate=" + this.createDate + ", id=" + this.id
		        + ", organizationDescription=" + this.organizationDescription + ", url=" + this.url + "]";
	}


}
