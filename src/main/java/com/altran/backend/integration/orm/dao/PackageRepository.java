/*
 * Property of arck23@gmail.com
 * Class PackageRepository.java created on 23 ene. 2020 21:55:00
 */
package com.altran.backend.integration.orm.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.altran.backend.integration.orm.entity.PackageEntity;

/**
 * Repository (DAO) dedicated to perform the basics methods to handle the operations with
 * {@link PackageEntity}. All the available operations are the inherited from {@link JpaRepository}
 * 
 * @author Jose
 * @since 1.0.0
 */
@Repository
public interface PackageRepository extends JpaRepository<PackageEntity, Long> {

}

