package com.altran.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * This class is dedicated to define custom configuration in a Rest application
 * 
 * @author Jose
 * @since 1.0.0
 */
@Configuration
public class WebRestConfiguration {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}


}
