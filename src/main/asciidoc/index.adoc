= Package API Documentation
:toc: left
:sectnums:

== Packages API
Collection of CRUD API endpoints used to get packages obtained from external server.

=== Get All Packages
Obtains all the packages.

==== Sample Request
include::{snippets}/packages/get-all/http-request.adoc[]

==== Sample Response
include::{snippets}/packages/get-all/http-response.adoc[]

==== CURL sample
include::{snippets}/packages/get-all/curl-request.adoc[]